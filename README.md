# Multilingual Wordpress

Make Wordpress multi language supported by Polylang plugin and following this instruction:

## Add Polylang Plugin

In Wordpress admin pannel's dashboard, from side menu select "Plugins" and after that click on "Add new" button.

Search "Polylang" and install it and then Active Polylang.

## Define a shortcode for language switcher widget and translating strings

Open "Plugin Editor" and select "Polylang" plugin to edit from select box and press on "Select" button.

Now you can see list of files and folders of Polylang plugin.

In list find "functions.php" (it's in "include" folder).

Open it and append this codes bottom of its content:

```php
// Language Switcher
function polylang_shortcode($attrs) {
	extract(shortcode_atts(array(
    	"flag" => 1,
		"name" => 0,
		"dropdown" => 0,
		"inline" => 1
    ), $attrs));
	ob_start();
	pll_the_languages(array("hide_if_no_translation"=>1,"hide_if_empty"=>1,"show_flags"=>$flag,"show_names"=>$name,"dropdown"=>$dropdown,"post_id"=>get_the_ID()));
	$langSwitcher = ob_get_clean();
	if ($inline === 1) {
		$inline = "inline";
	} else {
		$inline = "block";
	}
	$listTag = "";
	$listTagClose = "";
	if ($dropdown == 0) {
		$listTag = "<ul class='language-switcher $inline'>";
		$listTagClose = "</ul>";
	}
	return $listTag.$langSwitcher.$listTagClose;
}
add_shortcode("polylang-switch", "polylang_shortcode");


// String Translator
function polylang_translation_shortcode($attrs) {
	extract(shortcode_atts(array(
    	"text" => "",
		"lang" => pll_current_language()
    ), $attrs));

	$result = "";
	if ($lang === "") {
		$result = pll__(str_replace("_", " ", $text));
	} else {
		$result = pll_translate_string(str_replace("_", " ", $text), $lang);
	}
	
	return $result;
}
add_shortcode("polylang-text", "polylang_translation_shortcode");
```

You can define a class in your plugin stylesheets (`styles.css`) for `language-switcher`, `inline` and `block` class names

### Register new string translation

Open "functions.php" file of Polylang plugin in Wordpress plugin editor and append this code for each string:

```php
add_action('init', function() {
  pll_register_string('string_name', 'string_value');
});
```

If you use space in `string_value`, you have to use _ instead of space for shortcode text option's value

Example:

```php
add_action('init', function() {
  pll_register_string('Greeting', 'hello world');
});
```

After of registering a new string you can see it in Polylang's String Translations list that you can find it in
admin panel side menu >> Languages >> String translations
and write a translation for each language


### Use shortcodes

#### 1. Language Switcher

Everywhere you want to add language switcher you can write `[polylang-switch]` or `[polylang-switch {options}]`.

Options used by this form:
    `[polylang-switch option=value]`

You can use multiple options by adding a space between each option/value couple just like this:
    `[polylang-switch option1=value1 option2=value2]`

Values can be 0 or 1

##### Options:

- flag: Default=1
- name: Defalt=0
- dropdown: Default=0
- inline: Default=1

#### 2. String Translator

If you want to use translated strings you have to use this shortcode in your post/page/project/...:

`[polylang-text text=string_value]`

Value of text attributes should be `string_value` that you registered for your string.

As default this shortcode translates your string to current language, if you want to translate to other language you can use `lang` option in your shortcode like this:

`[polylang-text text=string_value lang=language_code]`

For example:
`[polylang-text text=hello_world lang=ar]`